package com.app.server.model.response;

import java.util.List;

import com.app.server.entity.Denominations;

public class DenominationResponse extends CommonResponse {

	private List<Denominations> denominations;

	public List<Denominations> getDenominations() {
		return denominations;
	}

	public void setDenominations(List<Denominations> denominations) {
		this.denominations = denominations;
	}
	
}
