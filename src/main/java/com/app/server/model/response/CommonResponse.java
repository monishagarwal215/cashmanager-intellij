package com.app.server.model.response;

import javax.persistence.Transient;

public class CommonResponse {
	@Transient
	private int status;
	@Transient
	private String message;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
