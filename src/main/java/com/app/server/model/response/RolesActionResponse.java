package com.app.server.model.response;

import com.app.server.entity.RoleActions;
import com.app.server.entity.Roles;

import java.util.List;

public class RolesActionResponse extends CommonResponse {
    private List<RoleActions> roleActionsList;

    public List<RoleActions> getRoleActionsList() {
        return roleActionsList;
    }

    public void setRoleActionsList(List<RoleActions> roleActionsList) {
        this.roleActionsList = roleActionsList;
    }
}
