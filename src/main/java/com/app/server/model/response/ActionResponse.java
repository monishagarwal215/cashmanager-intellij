package com.app.server.model.response;

import com.app.server.entity.Actions;
import com.app.server.entity.Roles;

import java.util.List;

public class ActionResponse extends CommonResponse {
    private List<Actions> actionsList;

    public List<Actions> getActionsList() {
        return actionsList;
    }

    public void setActionsList(List<Actions> actionsList) {
        this.actionsList = actionsList;
    }
}
