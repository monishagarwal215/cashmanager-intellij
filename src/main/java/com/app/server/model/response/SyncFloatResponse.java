package com.app.server.model.response;

import com.app.server.entity.CountHistory;
import com.app.server.entity.FloatHistory;

import java.util.List;

/**
 * Created by dinesh on 22/06/17.
 */
public class SyncFloatResponse extends CommonResponse {
    List<FloatHistory> floatHistoryList;

    public List<FloatHistory> getFloatHistoryList() {
        return floatHistoryList;
    }

    public void setFloatHistoryList(List<FloatHistory> floatHistoryList) {
        this.floatHistoryList = floatHistoryList;
    }
}
