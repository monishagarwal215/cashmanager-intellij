package com.app.server.model.response;

import com.app.server.entity.CountHistory;

import java.util.List;

/**
 * Created by dinesh on 22/06/17.
 */
public class SyncCountResponse extends CommonResponse {
    List<CountHistory> countHistoryList;

    public List<CountHistory> getCountHistoryList() {
        return countHistoryList;
    }

    public void setCountHistoryList(List<CountHistory> countHistoryList) {
        this.countHistoryList = countHistoryList;
    }
}
