package com.app.server.model.response;

import com.app.server.entity.Roles;

import java.util.List;

public class RolesResponse extends CommonResponse {
    private List<Roles> rolesList;

    public List<Roles> getRolesList() {
        return rolesList;
    }

    public void setRolesList(List<Roles> rolesList) {
        this.rolesList = rolesList;
    }
}
