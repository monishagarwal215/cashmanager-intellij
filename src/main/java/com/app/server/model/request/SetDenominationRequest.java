package com.app.server.model.request;

import com.app.server.entity.Denominations;

import java.util.List;

/**
 * Created by dinesh on 19/06/17.
 */
public class SetDenominationRequest {
    private String deviceId;
    private List<Denominations>  denominations;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public List<Denominations> getDenominations() {
        return denominations;
    }

    public void setDenominations(List<Denominations> denominations) {
        this.denominations = denominations;
    }
}
