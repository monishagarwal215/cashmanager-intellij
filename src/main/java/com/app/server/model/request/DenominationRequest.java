package com.app.server.model.request;

public class DenominationRequest {
	private String denominationName;
	private int denominationValue;
	private int defaultQuantity;
	private boolean isActive;

	public String getDenominationName() {
		return denominationName;
	}

	public void setDenominationName(String denominationName) {
		this.denominationName = denominationName;
	}

	public int getDenominationValue() {
		return denominationValue;
	}

	public void setDenominationValue(int denominationValue) {
		this.denominationValue = denominationValue;
	}

	public int getDefaultQuantity() {
		return defaultQuantity;
	}

	public void setDefaultQuantity(int defaultQuantity) {
		this.defaultQuantity = defaultQuantity;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

}
