package com.app.server.model.request;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class RegisterRequest {
    @NotNull(message = "Device Id key not available")
    @NotEmpty(message = "Device Id cannot be Empty")
    private String deviceId;
    @NotNull(message = "Device name key not available")
    @NotEmpty(message = "Device name cannot be empty")
    private String deviceName;
    @NotNull(message = "Device description key not available")
    @NotEmpty(message = "Device description cannot be empty")
    private String deviceDescription;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceDescription() {
        return deviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        this.deviceDescription = deviceDescription;
    }

}
