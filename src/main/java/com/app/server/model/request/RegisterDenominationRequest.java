package com.app.server.model.request;

import java.util.List;

import com.app.server.entity.Denominations;

public class RegisterDenominationRequest {

	private long deviceId;
	private List<Denominations> denominations;

	public long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(long deviceId) {
		this.deviceId = deviceId;
	}

	public List<Denominations> getDenominations() {
		return denominations;
	}

	public void setDenominations(List<Denominations> denominations) {
		this.denominations = denominations;
	}

}
