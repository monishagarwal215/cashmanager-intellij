package com.app.server.model.request;

import com.app.server.entity.Denominations;

import java.util.List;

/**
 * Created by dinesh on 19/06/17.
 */
public class CheckBalanceRequest {
    private String deviceId;
    private String countType;
    private List<Denominations> denominations;
    private int userCountValue;
    private int systemCountValue;

    public String getCountType() {
        return countType;
    }

    public void setCountType(String countType) {
        this.countType = countType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public List<Denominations> getDenominations() {
        return denominations;
    }

    public void setDenominations(List<Denominations> denominations) {
        this.denominations = denominations;
    }

    public int getUserCountValue() {
        return userCountValue;
    }

    public void setUserCountValue(int userCountValue) {
        this.userCountValue = userCountValue;
    }

    public int getSystemCountValue() {
        return systemCountValue;
    }

    public void setSystemCountValue(int systemCountValue) {
        this.systemCountValue = systemCountValue;
    }
}
