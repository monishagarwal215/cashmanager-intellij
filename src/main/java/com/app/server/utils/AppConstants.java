package com.app.server.utils;

/**
 * Created by dinesh on 20/06/17.
 */
public interface AppConstants {

    String DEVICE_ID_CANNOT_BE_EMPTY = "device.id.empty";
    String DEVICE_DOES_NOT_EXISTS = "device.does.not.exists";
    String COUNT_TYPE_EMPTY = "count.type.empty";
    String COUNT_TYPE_INVALID = "count.type.invalid";
    String DENOMINATION_LIST_EMPTY = "denomination.list.empty";
}
