package com.app.server.utils;

import java.util.Locale;
import java.util.ResourceBundle;

// TODO: Auto-generated Javadoc

/**
 * The Class Utility.
 */
public class Utility {

    /**
     * The locale.
     */
    public static Locale locale = new Locale("en", "US");

    /**
     * The b1.
     */
    public static ResourceBundle b1 = ResourceBundle.getBundle("messages", locale);


    /**
     * Gets the message.
     *
     * @param resourceIndex the resource index
     * @return the message
     */
    public static String getMessage(String resourceIndex) {

        if (b1.containsKey(resourceIndex)) {
            return b1.getString(resourceIndex);
        }

        return b1.getString(resourceIndex);
    }


}
