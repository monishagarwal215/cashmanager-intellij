package com.app.server.repo;

import com.app.server.entity.Denominations;
import com.app.server.entity.FloatHistory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IFloatHistoryRepository extends CrudRepository<FloatHistory, Integer> {

    FloatHistory findByFloatId(int id);

    FloatHistory findTopByRegisterIdOrderByFloatDate(int id);

    List<FloatHistory> findAllByRegisterId(int registerId);
}
