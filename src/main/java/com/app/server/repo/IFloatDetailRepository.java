package com.app.server.repo;

import com.app.server.entity.FloatDetails;
import com.app.server.entity.FloatHistory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IFloatDetailRepository extends CrudRepository<FloatDetails, Integer> {

    List<FloatDetails> findByFloatId(int id);
    //List<Denominations> findAll();

}
