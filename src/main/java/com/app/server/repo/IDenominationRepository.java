package com.app.server.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.app.server.entity.Denominations;
import com.app.server.entity.Register;
 
public interface IDenominationRepository extends CrudRepository<Denominations, Integer> {
 
	
	Denominations findByDenominationId(int id);
	
	List<Denominations> findAll();
 
}
