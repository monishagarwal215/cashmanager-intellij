package com.app.server.repo;

import com.app.server.entity.Register;
import com.app.server.entity.Roles;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IRolesRepository extends CrudRepository<Roles, Integer> {

    List<Roles> findAll();


}
