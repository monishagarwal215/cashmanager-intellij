package com.app.server.repo;

import com.app.server.entity.CountDetails;
import com.app.server.entity.FloatDetails;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ICountDetailRepository extends CrudRepository<CountDetails, Integer> {

    List<CountDetails> findByCountId(int id);

}
