package com.app.server.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.app.server.entity.Register;

public interface IRegisterRepository extends CrudRepository<Register, Integer> {

    //List<Register> findByYearLessThan(int year);

    Register findByDeviceId(String deviceId);


}
