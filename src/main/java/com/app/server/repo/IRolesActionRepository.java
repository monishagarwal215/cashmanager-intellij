package com.app.server.repo;

import com.app.server.entity.Actions;
import com.app.server.entity.RoleActions;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IRolesActionRepository extends CrudRepository<RoleActions, Integer> {

    List<RoleActions> findAll();


}
