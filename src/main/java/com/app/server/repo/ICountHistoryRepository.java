package com.app.server.repo;

import com.app.server.entity.CountHistory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ICountHistoryRepository extends CrudRepository<CountHistory, Integer> {

    CountHistory findByCountId(int id);

    CountHistory findTopByRegisterIdOrderByCountDate(int id);

    List<CountHistory> findAllByRegisterId(int id);
}
