package com.app.server.repo;

import com.app.server.entity.Denominations;
import com.app.server.entity.RegisterDenominations;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IRegisterDenominationRepository extends CrudRepository<RegisterDenominations, Integer> {


    List<RegisterDenominations> findByRegisterId(int registerId);

    //List<Denominations> findAll();

}
