package com.app.server.repo;

import com.app.server.entity.Actions;
import com.app.server.entity.Roles;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IActionsRepository extends CrudRepository<Actions, Integer> {

    List<Actions> findAll();


}
