package com.app.server.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "CountDetails")
public class CountDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 11)
    private int countDetailsId;
    @NotNull
    @Column(length = 11)
    private int countId;
    @NotNull
    @Column(length = 11)
    private int denominationId;
    @NotNull
    @Column(length = 25)
    private String denominationName;
    @NotNull
    @Column(length = 11)
    private int denominationValue;
    @NotNull
    @Column(length = 11)
    private int denominationQuantity;
    @NotNull
    @Column(length = 11)
    private int denominationTotal;
    @Temporal(TemporalType.TIMESTAMP)
    private Date syncDate;
    @Column(length = 36)
    private String deviceId;
    @Transient
    private Denominations denominations;
    @Transient
    private int serverId;
    @Transient
    private boolean synced;


    public int getCountDetailsId() {
        return countDetailsId;
    }

    public void setCountDetailsId(int countDetailsId) {
        this.countDetailsId = countDetailsId;
    }

    public int getCountId() {
        return countId;
    }

    public void setCountId(int countId) {
        this.countId = countId;
    }

    public int getDenominationId() {
        return denominationId;
    }

    public void setDenominationId(int denominationId) {
        this.denominationId = denominationId;
    }

    public String getDenominationName() {
        return denominationName;
    }

    public void setDenominationName(String denominationName) {
        this.denominationName = denominationName;
    }

    public int getDenominationValue() {
        return denominationValue;
    }

    public void setDenominationValue(int denominationValue) {
        this.denominationValue = denominationValue;
    }

    public int getDenominationQuantity() {
        return denominationQuantity;
    }

    public void setDenominationQuantity(int denominationQuantity) {
        this.denominationQuantity = denominationQuantity;
    }

    public int getDenominationTotal() {
        return denominationTotal;
    }

    public void setDenominationTotal(int denominationTotal) {
        this.denominationTotal = denominationTotal;
    }

    public Denominations getDenominations() {
        return denominations;
    }

    public void setDenominations(Denominations denominations) {
        this.denominations = denominations;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(Date syncDate) {
        this.syncDate = syncDate;
    }
}
