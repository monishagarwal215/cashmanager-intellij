package com.app.server.entity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "FloatDetails")
public class FloatDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int floatDetailsId;
    @NotNull
    @Column(length = 11)
    private int floatId;
    @NotNull
    @Column(length = 11)
    private int denominationId;
    @NotNull
    @Column(length = 25)
    private String denominationName;
    @NotNull
    @Column(length = 11)
    private int denominationValue;
    @NotNull
    @Column(length = 11)
    private int denominationQuantity;
    @NotNull
    @Column(length = 11)
    private int denominationTotal;
    @Column(length = 36)
    private String deviceId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date syncDate;
    @Transient
    private Denominations denominations;
    @Transient
    private int serverId;
    @Transient
    private boolean synced;


    public int getFloatDetailsId() {
        return floatDetailsId;
    }

    public void setFloatDetailsId(int floatDetailsId) {
        this.floatDetailsId = floatDetailsId;
    }

    public int getFloatId() {
        return floatId;
    }

    public void setFloatId(int floatId) {
        this.floatId = floatId;
    }

    public int getDenominationId() {
        return denominationId;
    }

    public void setDenominationId(int denominationId) {
        this.denominationId = denominationId;
    }

    public String getDenominationName() {
        return denominationName;
    }

    public void setDenominationName(String denominationName) {
        this.denominationName = denominationName;
    }

    public int getDenominationValue() {
        return denominationValue;
    }

    public void setDenominationValue(int denominationValue) {
        this.denominationValue = denominationValue;
    }

    public int getDenominationQuantity() {
        return denominationQuantity;
    }

    public void setDenominationQuantity(int denominationQuantity) {
        this.denominationQuantity = denominationQuantity;
    }

    public int getDenominationTotal() {
        return denominationTotal;
    }

    public void setDenominationTotal(int denominationTotal) {
        this.denominationTotal = denominationTotal;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }


    public Denominations getDenominations() {
        return denominations;
    }

    public void setDenominations(Denominations denominations) {
        this.denominations = denominations;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(Date syncDate) {
        this.syncDate = syncDate;
    }
}
