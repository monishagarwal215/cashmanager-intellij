package com.app.server.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "RoleActions")
public class RoleActions {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 11)
    private int roleActionId;

    @NotNull
    @Column(length = 11)
    private int roleId;

    @NotNull
    @Column(length = 11)
    private int actionId;

    @Column(nullable = false, columnDefinition = "TINYINT DEFAULT 1", length = 4)
    private boolean isActive = true;

    @NotNull
    @Column(length = 11)
    private int canModify;

    public int getCanModify() {
        return canModify;
    }

    public void setCanModify(int canModify) {
        this.canModify = canModify;
    }

    public int getRoleActionId() {
        return roleActionId;
    }

    public void setRoleActionId(int roleActionId) {
        this.roleActionId = roleActionId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
