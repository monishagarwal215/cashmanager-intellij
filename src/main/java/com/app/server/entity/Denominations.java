package com.app.server.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Denominations")
public class Denominations {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 11)
    private int denominationId;
    @NotNull
    @Column(length = 25)
    private String denominationName;
    @Column(length = 30)
    private String denominationDescription;
    @Column(length = 11)
    private int sortOrder;
    @NotNull
    @Column(length = 11)
    private int denominationValue;
    @NotNull
    @Column(length = 11)
    private int defaultQuantity;
    @Column(nullable = false, columnDefinition = "TINYINT DEFAULT 1", length = 4)
    private boolean isActive;


    public int getDenominationId() {
        return denominationId;
    }

    public void setDenominationId(int denominationId) {
        this.denominationId = denominationId;
    }

    public String getDenominationName() {
        return denominationName;
    }

    public void setDenominationName(String denominationName) {
        this.denominationName = denominationName;
    }

    public int getDenominationValue() {
        return denominationValue;
    }

    public void setDenominationValue(int denominationValue) {
        this.denominationValue = denominationValue;
    }

    public int getDefaultQuantity() {
        return defaultQuantity;
    }

    public void setDefaultQuantity(int defaultQuantity) {
        this.defaultQuantity = defaultQuantity;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getDenominationDescription() {
        return denominationDescription;
    }

    public void setDenominationDescription(String denominationDescription) {
        this.denominationDescription = denominationDescription;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }
}
