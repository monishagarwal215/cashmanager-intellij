package com.app.server.entity;

import org.springframework.context.annotation.Primary;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "Registers")
public class Register {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 11)
    private int registerId;

    @NotNull
    @Column(unique = true, length = 36)
    private String deviceId;

    @NotNull
    @Column(length = 50)
    private String deviceName;

    @Column(length = 150)
    private String deviceDescription;

    @Column(length = 11)
    private int adjustmentValue;

    @Temporal(TemporalType.TIMESTAMP)
    private Date adjustmentDate;

    @Column(length = 50)
    private String adjustmentRecordedBy;

    @NotNull
    @Column(length = 50)
    private String createdBy;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @NotNull
    @Column(length = 50)
    private String updatedBy;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

    public int getRegisterId() {
        return registerId;
    }

    public void setRegisterId(int registerId) {
        this.registerId = registerId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceDescription() {
        return deviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        this.deviceDescription = deviceDescription;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }


    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
}
