package com.app.server.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "CountHistory")
public class CountHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 11)
    private int countId;
    @Column(length = 50)
    @NotNull
    private String countRecordedBy;
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date countDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date syncDate;
    @NotNull
    @Column(length = 20)
    private String countType;
    @Column(length = 11)
    @NotNull
    private int registerId;
    @Column(length = 11)
    @NotNull
    private int userCountValue;
    @Column(length = 11)
    @NotNull
    private int systemCountValue;
    @Column(length = 36)
    private String deviceId;
    @Transient
    private List<CountDetails> countDetails;
    @Transient
    private int serverId;
    @Transient
    private boolean synced;
    @Column(length = 2048)
    private String notes;


    public int getCountId() {
        return countId;
    }

    public void setCountId(int countId) {
        this.countId = countId;
    }

    public String getCountRecordedBy() {
        return countRecordedBy;
    }

    public void setCountRecordedBy(String countRecordedBy) {
        this.countRecordedBy = countRecordedBy;
    }

    public Date getCountDate() {
        return countDate;
    }

    public void setCountDate(Date countDate) {
        this.countDate = countDate;
    }

    public String getCountType() {
        return countType;
    }

    public void setCountType(String countType) {
        this.countType = countType;
    }

    public int getRegisterId() {
        return registerId;
    }

    public void setRegisterId(int registerId) {
        this.registerId = registerId;
    }

    public int getUserCountValue() {
        return userCountValue;
    }

    public void setUserCountValue(int userCountValue) {
        this.userCountValue = userCountValue;
    }

    public int getSystemCountValue() {
        return systemCountValue;
    }

    public void setSystemCountValue(int systemCountValue) {
        this.systemCountValue = systemCountValue;
    }

    public List<CountDetails> getCountDetails() {
        return countDetails;
    }

    public void setCountDetails(List<CountDetails> countDetails) {
        this.countDetails = countDetails;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(Date syncDate) {
        this.syncDate = syncDate;
    }
}
