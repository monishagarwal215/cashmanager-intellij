package com.app.server.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RegisterDenominations")
public class RegisterDenominations {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int registerDenominationId;
    private int registerId;
    private int denominationId;
    private int registerDenominationsCount;

    public int getRegisterDenominationId() {
        return registerDenominationId;
    }

    public void setRegisterDenominationId(int registerDenominationId) {
        this.registerDenominationId = registerDenominationId;
    }

    public int getRegisterId() {
        return registerId;
    }

    public void setRegisterId(int registerId) {
        this.registerId = registerId;
    }

    public int getDenominationId() {
        return denominationId;
    }

    public void setDenominationId(int denominationId) {
        this.denominationId = denominationId;
    }

    public int getRegisterDenominationsCount() {
        return registerDenominationsCount;
    }

    public void setRegisterDenominationsCount(int registerDenominationsCount) {
        this.registerDenominationsCount = registerDenominationsCount;
    }

}
