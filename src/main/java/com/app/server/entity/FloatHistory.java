package com.app.server.entity;

import com.app.server.model.response.CommonResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "FloatHistory")
public class FloatHistory extends CommonResponse {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 11)
    private int floatId;
    @Column(length = 50)
    private String floatSetBy;
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date floatDate;
    @Column(length = 11)
    @NotNull
    private int registerId;
    @NotNull
    @Column(length = 20)
    private String floatType;
    @Column(length = 11)
    @NotNull
    private int floatValue;
    @Column(length = 36)
    private String deviceId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date syncDate;
    @Transient
    private List<FloatDetails> floatDetails;
    @Transient
    private int serverId;
    @Transient
    private boolean synced;

    public int getFloatId() {
        return floatId;
    }

    public void setFloatId(int floatId) {
        this.floatId = floatId;
    }

    public String getFloatSetBy() {
        return floatSetBy;
    }

    public void setFloatSetBy(String floatSetBy) {
        this.floatSetBy = floatSetBy;
    }

    public Date getFloatDate() {
        return floatDate;
    }

    public void setFloatDate(Date floatDate) {
        this.floatDate = floatDate;
    }

    public int getRegisterId() {
        return registerId;
    }

    public void setRegisterId(int registerId) {
        this.registerId = registerId;
    }

    public int getFloatValue() {
        return floatValue;
    }

    public void setFloatValue(int floatValue) {
        this.floatValue = floatValue;
    }

    public List<FloatDetails> getFloatDetails() {
        return floatDetails;
    }

    public void setFloatDetails(List<FloatDetails> floatDetails) {
        this.floatDetails = floatDetails;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(Date syncDate) {
        this.syncDate = syncDate;
    }

    public String getFloatType() {
        return floatType;
    }

    public void setFloatType(String floatType) {
        this.floatType = floatType;
    }
}
