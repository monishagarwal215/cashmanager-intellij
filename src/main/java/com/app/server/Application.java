package com.app.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@EnableWebMvc
@SpringBootApplication
public class Application {

    static final Logger logger = LogManager.getLogger(Application.class.getName());

    public static void main(String[] args) {
        logger.info("entered application");
        SpringApplication.run(Application.class, args);
    }
}
