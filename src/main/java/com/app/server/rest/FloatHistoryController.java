package com.app.server.rest;

import com.app.server.entity.Denominations;
import com.app.server.entity.FloatDetails;
import com.app.server.entity.FloatHistory;
import com.app.server.entity.Register;
import com.app.server.model.request.SetFloatRequest;
import com.app.server.model.response.CommonResponse;
import com.app.server.model.response.DenominationResponse;
import com.app.server.model.response.RegisterResponse;
import com.app.server.model.response.SyncFloatResponse;
import com.app.server.repo.*;
import com.app.server.utils.AppConstants;
import com.app.server.utils.StringUtils;
import com.app.server.utils.Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RequestMapping("/api")
@RestController
public class FloatHistoryController {

    @Autowired
    private IFloatHistoryRepository iFloatHistoryRepository;

    @Autowired
    private IFloatDetailRepository iFloatDetailRepository;

    @Autowired
    private IDenominationRepository iDenominationRepository;

    @Autowired
    private IRegisterRepository iRegisterRepository;

    @Autowired
    private IRegisterDenominationRepository iRegisterDenominationRepository;

    static final Logger logger = LogManager.getLogger(FloatHistoryController.class.getName());

    // CREATE
    @PostMapping("/setFloat")
    @ResponseBody
    public ResponseEntity<CommonResponse> setFloat(@RequestBody SetFloatRequest setFloatRequest) {

        CommonResponse commonResponse = new CommonResponse();
        if (StringUtils.isNullOrEmpty(setFloatRequest.getDeviceId())) {
            commonResponse.setStatus(0);
            commonResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_ID_CANNOT_BE_EMPTY));
            return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
        }

        try {
            Register register = iRegisterRepository.findByDeviceId(setFloatRequest.getDeviceId());
            if (register == null) {
                commonResponse.setStatus(0);
                commonResponse.setMessage("Device does not exits with this id");
                return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
            } else if (setFloatRequest.getDenominations() != null && setFloatRequest.getDenominations().size() > 0) {


                FloatHistory floatHistory = new FloatHistory();
                floatHistory.setFloatDate(new Date(System.currentTimeMillis()));
                floatHistory.setSyncDate(new Date(System.currentTimeMillis()));
                floatHistory.setDeviceId(register.getDeviceId());
                floatHistory.setRegisterId(register.getRegisterId());
                floatHistory = iFloatHistoryRepository.save(floatHistory);

                List<FloatDetails> floatDetailList = new ArrayList<>();
                int floatTotal = 0;
                for (Denominations temp : setFloatRequest.getDenominations()) {
                    FloatDetails floatDetail = new FloatDetails();
                    floatDetail.setFloatId(floatHistory.getFloatId());
                    floatDetail.setSyncDate(new Date(System.currentTimeMillis()));
                    floatDetail.setDeviceId(setFloatRequest.getDeviceId());
                    floatDetail.setDenominationId(temp.getDenominationId());
                    floatDetail.setDenominationName(temp.getDenominationName());
                    floatDetail.setDenominationQuantity(temp.getDefaultQuantity());
                    floatDetail.setDenominationTotal(temp.getDenominationValue() * temp.getDefaultQuantity());
                    floatDetail.setDenominationValue(temp.getDenominationValue());
                    floatTotal = floatTotal + temp.getDenominationValue() * temp.getDefaultQuantity();

                    floatDetailList.add(floatDetail);
                }
                floatHistory.setFloatValue(floatTotal);
                iFloatDetailRepository.save(floatDetailList);
                iFloatHistoryRepository.save(floatHistory);

                commonResponse.setStatus(1);
                commonResponse.setMessage("Float is successfully updated.");
                return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.OK);

            } else {
                commonResponse.setMessage("Denomination List cannot be empty");
                return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<CommonResponse>(HttpStatus.BAD_REQUEST);
        }


    }

    @GetMapping("/getLatestFloat")
    @ResponseBody
    public ResponseEntity<DenominationResponse> getLatestFloat(String deviceId) {

        DenominationResponse denominationResponse = new DenominationResponse();
        if (StringUtils.isNullOrEmpty(deviceId)) {
            denominationResponse.setStatus(0);
            denominationResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_ID_CANNOT_BE_EMPTY));
            return new ResponseEntity<DenominationResponse>(denominationResponse, HttpStatus.BAD_REQUEST);
        }
        try {
            Register register = iRegisterRepository.findByDeviceId(deviceId);
            if (register == null) {
                denominationResponse.setStatus(0);
                denominationResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_DOES_NOT_EXISTS));
                return new ResponseEntity<DenominationResponse>(denominationResponse, HttpStatus.BAD_REQUEST);
            } else {
                FloatHistory floatHistory = iFloatHistoryRepository.findTopByRegisterIdOrderByFloatDate(register.getRegisterId());
                if (floatHistory != null) {
                    List<FloatDetails> floatDetails = iFloatDetailRepository.findByFloatId(floatHistory.getFloatId());
                    List<Denominations> denominationList = new ArrayList<>();
                    for (FloatDetails temp : floatDetails) {
                        Denominations denominations = new Denominations();
                        denominations.setDefaultQuantity(temp.getDenominationQuantity());
                        denominations.setActive(true);
                        denominations.setDenominationId(temp.getDenominationId());
                        denominations.setDenominationName(temp.getDenominationName());
                        denominations.setDenominationValue(temp.getDenominationValue());
                        denominationList.add(denominations);
                    }
                    denominationResponse.setStatus(1);
                    denominationResponse.setMessage("Float history fetched sucessfully");
                    denominationResponse.setDenominations(denominationList);
                    return new ResponseEntity<DenominationResponse>(denominationResponse, HttpStatus.OK);
                } else {
                    denominationResponse.setStatus(0);
                    denominationResponse.setMessage("There is not float history by this device Id");
                    return new ResponseEntity<DenominationResponse>(denominationResponse, HttpStatus.BAD_REQUEST);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            denominationResponse.setMessage("Bad Parameter");
            denominationResponse.setStatus(0);
            return new ResponseEntity<DenominationResponse>(denominationResponse, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/syncFloatHistory")
    @ResponseBody
    public ResponseEntity<SyncFloatResponse> getFloatHistory(String deviceId) {

        SyncFloatResponse syncFloatResponse = new SyncFloatResponse();
        if (StringUtils.isNullOrEmpty(deviceId)) {
            syncFloatResponse.setStatus(0);
            syncFloatResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_ID_CANNOT_BE_EMPTY));
            return new ResponseEntity<SyncFloatResponse>(syncFloatResponse, HttpStatus.BAD_REQUEST);
        }
        try {
            Register register = iRegisterRepository.findByDeviceId(deviceId);
            if (register == null) {
                syncFloatResponse.setStatus(0);
                syncFloatResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_DOES_NOT_EXISTS));
                return new ResponseEntity<SyncFloatResponse>(syncFloatResponse, HttpStatus.BAD_REQUEST);
            } else {
                List<FloatHistory> floatHistoryList = iFloatHistoryRepository.findAllByRegisterId(register.getRegisterId());
                if (floatHistoryList != null && floatHistoryList.size() > 0) {
                    for (FloatHistory temp : floatHistoryList) {
                        List<FloatDetails> countDetails = iFloatDetailRepository.findByFloatId(temp.getFloatId());
                        //convert floatId to serverid and make zero
                        temp.setServerId((int) temp.getFloatId());
                        temp.setSynced(true);
                        for (FloatDetails temp1 : countDetails) {
                            temp1.setServerId((int) temp1.getFloatDetailsId());
                            Denominations denominations = new Denominations();
                            denominations.setDenominationValue(temp1.getDenominationValue());
                            denominations.setDenominationName(temp1.getDenominationName());
                            denominations.setDenominationId(temp1.getDenominationId());
                            denominations.setActive(true);
                            denominations.setDefaultQuantity(temp1.getDenominationQuantity());
                            temp1.setDenominations(denominations);
                            temp1.setSynced(true);
                        }
                        temp.setFloatDetails(countDetails);
                    }
                    syncFloatResponse.setFloatHistoryList(floatHistoryList);
                    syncFloatResponse.setStatus(1);
                    syncFloatResponse.setMessage("Float history fetched sucessfully");
                    return new ResponseEntity<SyncFloatResponse>(syncFloatResponse, HttpStatus.OK);
                } else {
                    syncFloatResponse.setStatus(0);
                    syncFloatResponse.setMessage("There is no float history by this device Id");
                    return new ResponseEntity<SyncFloatResponse>(syncFloatResponse, HttpStatus.BAD_REQUEST);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            syncFloatResponse.setMessage("Bad Parameter");
            syncFloatResponse.setStatus(0);
            return new ResponseEntity<SyncFloatResponse>(syncFloatResponse, HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping("/syncFloatHistory")
    @ResponseBody
    public ResponseEntity<FloatHistory> postFloatHistory(@Valid @RequestBody FloatHistory floatHistory, BindingResult bindingResult) {

        /*if (bindingResult.hasErrors()) {
            logger.debug("error");
            floatHistory.setStatus(0);
            floatHistory.setMessage(bindingResult.getAllErrors().get(0).getDefaultMessage());
            return new ResponseEntity<FloatHistory>(floatHistory, HttpStatus.OK);
        }*/

        try {
            floatHistory.setFloatId(0);
            floatHistory.setSyncDate(new Date(System.currentTimeMillis()));
            if (StringUtils.isNullOrEmpty(floatHistory.getFloatType())) {
                floatHistory.setFloatType("NA");
            }
            if (StringUtils.isNullOrEmpty(floatHistory.getFloatSetBy())) {
                floatHistory.setFloatSetBy("NA");
            }
            if (floatHistory.getFloatDate() == null) {
                floatHistory.setFloatDate(new Date());
            }
            FloatHistory floatHistorySaved = iFloatHistoryRepository.save(floatHistory);
            if (floatHistorySaved != null) {
                floatHistory.setServerId((int) floatHistorySaved.getFloatId());
                floatHistory.setSynced(true);
            }

            if (floatHistory.getFloatDetails() != null && floatHistory.getFloatDetails().size() > 0) {
                for (int i = 0; i <= floatHistory.getFloatDetails().size() - 1; i++) {
                    Denominations denominations = floatHistory.getFloatDetails().get(i).getDenominations();
                    if (denominations != null) {
                        floatHistory.getFloatDetails().get(i).setFloatId(floatHistorySaved.getFloatId());
                        floatHistory.getFloatDetails().get(i).setDenominationValue(denominations.getDenominationValue());
                        floatHistory.getFloatDetails().get(i).setDenominationQuantity(denominations.getDefaultQuantity());
                        floatHistory.getFloatDetails().get(i).setDenominationName(denominations.getDenominationName());
                        floatHistory.getFloatDetails().get(i).setDenominationId(denominations.getDenominationId());
                        floatHistory.getFloatDetails().get(i).setDenominationTotal(denominations.getDenominationValue() * denominations.getDefaultQuantity());
                        floatHistory.getFloatDetails().get(i).setSyncDate(new Date(System.currentTimeMillis()));
                    }
                    floatHistory.getFloatDetails().get(i).setFloatDetailsId(0);
                    FloatDetails floatDetailSaved = iFloatDetailRepository.save(floatHistory.getFloatDetails().get(i));
                    if (floatDetailSaved != null) {
                        floatHistory.getFloatDetails().get(i).setServerId((int) floatDetailSaved.getFloatDetailsId());
                        floatHistory.getFloatDetails().get(i).setSyncDate(floatDetailSaved.getSyncDate());
                        floatHistory.getFloatDetails().get(i).setSynced(true);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return new ResponseEntity<FloatHistory>(floatHistory, HttpStatus.OK);
    }
}