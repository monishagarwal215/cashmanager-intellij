package com.app.server.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.server.entity.Denominations;
import com.app.server.model.request.DenominationRequest;
import com.app.server.model.response.CommonResponse;
import com.app.server.model.response.DenominationResponse;
import com.app.server.repo.IDenominationRepository;

@RequestMapping("/api")
@RestController
@Controller
public class DenominationController {

	@Autowired
	private IDenominationRepository repo;

	static final Logger logger = LogManager.getLogger(DenominationController.class.getName());

	// CREATE
	@PostMapping("/denomination/create")
	@ResponseBody
	public ResponseEntity<CommonResponse> createDenomination(@RequestBody DenominationRequest denominationRequest) {

		Denominations denominations = new Denominations();
		denominations.setActive(denominationRequest.isActive());
		denominations.setDefaultQuantity(denominationRequest.getDefaultQuantity());
		denominations.setDenominationName(denominationRequest.getDenominationName());
		denominations.setDenominationValue(denominationRequest.getDenominationValue());

		CommonResponse commonResponse = new CommonResponse();
		try {
			repo.save(denominations);
		} catch (Exception e) {
			logger.error(e.getMessage());
			commonResponse.setMessage("Some Error found");
			commonResponse.setStatus(0);
			return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
		}

		commonResponse.setMessage("Successfully saved");
		commonResponse.setStatus(1);

		return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.OK);
	}

	// READ
	@GetMapping("/allDenomination")
	@ResponseBody
	public ResponseEntity<DenominationResponse> getAllDenominations() {
		DenominationResponse denominationResponse = new DenominationResponse();
		List<Denominations> denominations = null;
		try {
			denominations = repo.findAll();

		} catch (Exception e) {
			logger.error(e.getMessage());
			denominationResponse.setMessage("Bad Parameter");
			denominationResponse.setStatus(0);
			return new ResponseEntity<DenominationResponse>(denominationResponse, HttpStatus.BAD_REQUEST);
		}
		if (denominations == null) {
			denominationResponse.setMessage("No denomination found");
			denominationResponse.setStatus(0);
			return new ResponseEntity<DenominationResponse>(denominationResponse, HttpStatus.BAD_REQUEST);
		} else {
			denominationResponse.setDenominations(denominations);
			denominationResponse.setMessage("Fetched all denominations");
			denominationResponse.setStatus(1);
			return new ResponseEntity<DenominationResponse>(denominationResponse, HttpStatus.OK);
		}
	}
}