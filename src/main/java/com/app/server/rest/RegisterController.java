package com.app.server.rest;

import com.app.server.entity.Register;
import com.app.server.model.request.RegisterRequest;
import com.app.server.model.response.CommonResponse;
import com.app.server.model.response.RegisterResponse;
import com.app.server.repo.IRegisterRepository;
import com.app.server.utils.AppConstants;
import com.app.server.utils.StringUtils;
import com.app.server.utils.Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RequestMapping("/api")
@RestController
public class RegisterController {

    @Autowired
    private IRegisterRepository repo;

    static final Logger logger = LogManager.getLogger(RegisterController.class.getName());

    // CREATE
    @PostMapping("/register/create")
    @ResponseBody
    public ResponseEntity<RegisterResponse> createRegister(@Valid @RequestBody RegisterRequest registerRequest, BindingResult bindingResult) {

        RegisterResponse registerResponse = new RegisterResponse();
        if (bindingResult.hasErrors()) {
            logger.debug("error");
            registerResponse.setStatus(0);
            registerResponse.setMessage(bindingResult.getAllErrors().get(0).getDefaultMessage());
            return new ResponseEntity<RegisterResponse>(registerResponse, HttpStatus.BAD_REQUEST);
        }

        Register register = new Register();
        register.setDeviceName(registerRequest.getDeviceName());
        register.setDeviceDescription(registerRequest.getDeviceDescription());
        register.setDeviceId(registerRequest.getDeviceId());
        register.setCreatedDate(new Date());
        register.setUpdatedDate(new Date());
        register.setCreatedBy("Admin");
        register.setUpdatedBy("Admin");
        try {
            Register registerFound = repo.findByDeviceId(register.getDeviceId());
            if (registerFound != null) {
                registerFound.setDeviceName(registerRequest.getDeviceName());
                registerFound.setDeviceDescription(registerRequest.getDeviceDescription());
                registerFound.setDeviceId(registerRequest.getDeviceId());
                registerFound.setCreatedDate(new Date());
                registerFound.setUpdatedDate(new Date());
                registerFound.setCreatedBy("Admin");
                registerFound.setUpdatedBy("Admin");

                register = repo.save(registerFound);
            } else {
                register = repo.save(register);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<RegisterResponse>(HttpStatus.BAD_REQUEST);
        }
        registerResponse.setRegisterId(register.getRegisterId());
        registerResponse.setDeviceDescription(register.getDeviceDescription());
        registerResponse.setDeviceId(register.getDeviceId());
        registerResponse.setDeviceName(register.getDeviceName());
        registerResponse.setMessage("Successfully saved");
        registerResponse.setStatus(1);

        return new ResponseEntity<RegisterResponse>(registerResponse, HttpStatus.OK);
    }

    // READ
    @GetMapping("/register/read")
    @ResponseBody
    public ResponseEntity<RegisterResponse> readRegister(String deviceId) {
        Register register;
        RegisterResponse registerResponse = new RegisterResponse();
        if (StringUtils.isNullOrEmpty(deviceId)) {
            registerResponse.setStatus(0);
            registerResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_ID_CANNOT_BE_EMPTY));
            return new ResponseEntity<RegisterResponse>(registerResponse, HttpStatus.BAD_REQUEST);
        }

        try {
            register = repo.findByDeviceId(deviceId);
        } catch (Exception e) {
            logger.error(e.getMessage());
            registerResponse.setMessage("Bad Parameter");
            registerResponse.setStatus(0);
            return new ResponseEntity<RegisterResponse>(registerResponse, HttpStatus.BAD_REQUEST);
        }
        if (register == null) {
            registerResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_DOES_NOT_EXISTS));
            registerResponse.setStatus(0);
            return new ResponseEntity<RegisterResponse>(registerResponse, HttpStatus.BAD_REQUEST);
        } else {
            registerResponse.setDeviceDescription(register.getDeviceDescription());
            registerResponse.setDeviceId(register.getDeviceId());
            registerResponse.setDeviceName(register.getDeviceName());
            registerResponse.setMessage("Register successfully fetched");
            registerResponse.setStatus(1);
            return new ResponseEntity<RegisterResponse>(registerResponse, HttpStatus.OK);
        }
    }
}