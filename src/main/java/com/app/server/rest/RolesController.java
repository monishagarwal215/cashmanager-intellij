package com.app.server.rest;

import com.app.server.entity.Actions;
import com.app.server.entity.RoleActions;
import com.app.server.entity.Roles;
import com.app.server.model.response.ActionResponse;
import com.app.server.model.response.RolesActionResponse;
import com.app.server.model.response.RolesResponse;
import com.app.server.repo.IActionsRepository;
import com.app.server.repo.IRolesActionRepository;
import com.app.server.repo.IRolesRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/api")
@RestController
public class RolesController {

    @Autowired
    private IRolesRepository rolesRepository;

    @Autowired
    private IRolesActionRepository rolesActionRepository;

    @Autowired
    private IActionsRepository actionsRepository;

    static final Logger logger = LogManager.getLogger(RolesController.class.getName());


    // READ
    @GetMapping("/roles")
    @ResponseBody
    public ResponseEntity<RolesResponse> readRoles() {
        RolesResponse rolesResponse = new RolesResponse();
        try {
            List<Roles> rolesList = rolesRepository.findAll();
            rolesResponse.setRolesList(rolesList);
            rolesResponse.setMessage("Roles successfully fetched.");
            rolesResponse.setStatus(1);
            return new ResponseEntity<RolesResponse>(rolesResponse, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            rolesResponse.setMessage("Some error occured");
            rolesResponse.setStatus(0);
            return new ResponseEntity<RolesResponse>(rolesResponse, HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/actions")
    @ResponseBody
    public ResponseEntity<ActionResponse> readActions() {
        ActionResponse actionResponse = new ActionResponse();
        try {
            List<Actions> actionsList = actionsRepository.findAll();
            actionResponse.setActionsList(actionsList);
            actionResponse.setMessage("Roles successfully fetched.");
            actionResponse.setStatus(1);
            return new ResponseEntity<ActionResponse>(actionResponse, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            actionResponse.setMessage("Some error occured");
            actionResponse.setStatus(0);
            return new ResponseEntity<ActionResponse>(actionResponse, HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/roleAction")
    @ResponseBody
    public ResponseEntity<RolesActionResponse> readRolesActions() {
        RolesActionResponse rolesActionResponse = new RolesActionResponse();
        try {
            List<RoleActions> roleActionsList = rolesActionRepository.findAll();
            rolesActionResponse.setRoleActionsList(roleActionsList);
            rolesActionResponse.setMessage("Roles successfully fetched.");
            rolesActionResponse.setStatus(1);
            return new ResponseEntity<RolesActionResponse>(rolesActionResponse, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            rolesActionResponse.setMessage("Some error occured");
            rolesActionResponse.setStatus(0);
            return new ResponseEntity<RolesActionResponse>(rolesActionResponse, HttpStatus.BAD_REQUEST);
        }
    }
}