package com.app.server.rest;

import com.app.server.entity.Denominations;
import com.app.server.entity.Register;
import com.app.server.entity.RegisterDenominations;
import com.app.server.model.request.SetDenominationRequest;
import com.app.server.model.response.CommonResponse;
import com.app.server.model.response.DenominationResponse;
import com.app.server.model.response.RegisterResponse;
import com.app.server.repo.IDenominationRepository;
import com.app.server.repo.IRegisterDenominationRepository;
import com.app.server.repo.IRegisterRepository;
import com.app.server.utils.AppConstants;
import com.app.server.utils.StringUtils;
import com.app.server.utils.Utility;
import io.swagger.annotations.Api;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/api")
@RestController
public class RegisterDenominationController {

    @Autowired
    private IDenominationRepository iDenominationRepository;

    @Autowired
    private IRegisterRepository iRegisterRepository;

    @Autowired
    private IRegisterDenominationRepository iRegisterDenominationRepository;

    static final Logger logger = LogManager.getLogger(RegisterDenominationController.class.getName());

    // CREATE
    @PostMapping("/register/setDenomination")
    @ResponseBody
    public ResponseEntity<CommonResponse> setDenomination(@RequestBody SetDenominationRequest setDenominationRequest) {


        CommonResponse commonResponse = new CommonResponse();
        if (StringUtils.isNullOrEmpty(setDenominationRequest.getDeviceId())) {
            commonResponse.setStatus(1);
            commonResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_ID_CANNOT_BE_EMPTY));
            return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
        }

        try {
            Register register = iRegisterRepository.findByDeviceId(setDenominationRequest.getDeviceId());
            if (register == null) {
                commonResponse.setStatus(1);
                commonResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_DOES_NOT_EXISTS));
                return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
            } else if (setDenominationRequest.getDenominations() != null && setDenominationRequest.getDenominations().size() > 0) {
                for (Denominations temp : setDenominationRequest.getDenominations()) {
                    RegisterDenominations registerDenominations = new RegisterDenominations();
                    registerDenominations.setDenominationId(temp.getDenominationId());
                    registerDenominations.setRegisterId(register.getRegisterId());
                    registerDenominations.setRegisterDenominationsCount(temp.getDefaultQuantity());
                    iRegisterDenominationRepository.save(registerDenominations);
                }
                commonResponse.setStatus(1);
                commonResponse.setMessage("Successfully saved");
                return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.OK);

            } else {
                commonResponse.setMessage("Denomination List cannot be empty");
                return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<CommonResponse>(HttpStatus.BAD_REQUEST);
        }


    }

    // READ
    @GetMapping("/register/getDenominationByDevice")
    @ResponseBody
    public ResponseEntity<DenominationResponse> getDenominationByDeviceId(String deviceId) {
        DenominationResponse denominationResponse = new DenominationResponse();
        if (StringUtils.isNullOrEmpty(deviceId)) {
            denominationResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_ID_CANNOT_BE_EMPTY));
            return new ResponseEntity<DenominationResponse>(denominationResponse, HttpStatus.BAD_REQUEST);
        }
        try {
            Register register = iRegisterRepository.findByDeviceId(deviceId);
            if (register == null) {
                denominationResponse.setStatus(0);
                denominationResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_DOES_NOT_EXISTS));
                return new ResponseEntity<DenominationResponse>(denominationResponse, HttpStatus.BAD_REQUEST);
            } else {
                List<RegisterDenominations> registerDenominations = iRegisterDenominationRepository.findByRegisterId(register.getRegisterId());
                List<Denominations> denominations = new ArrayList<>();
                for (RegisterDenominations temp : registerDenominations) {
                    Denominations denomination = iDenominationRepository.findByDenominationId(temp.getDenominationId());
                    if (denomination != null) {
                        denomination.setDefaultQuantity(temp.getRegisterDenominationsCount());
                        denominations.add(denomination);
                    }
                }
                denominationResponse.setDenominations(denominations);
                denominationResponse.setStatus(1);
                denominationResponse.setMessage("Fetched Successfully");
                return new ResponseEntity<DenominationResponse>(denominationResponse, HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            denominationResponse.setMessage("Bad Parameter");
            return new ResponseEntity<DenominationResponse>(denominationResponse, HttpStatus.BAD_REQUEST);
        }
    }
}