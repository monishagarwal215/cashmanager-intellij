package com.app.server.rest;

import com.app.server.entity.*;
import com.app.server.model.request.CheckBalanceRequest;
import com.app.server.model.response.CommonResponse;
import com.app.server.model.response.SyncCountResponse;
import com.app.server.repo.*;
import com.app.server.utils.AppConstants;
import com.app.server.utils.CountType;
import com.app.server.utils.StringUtils;
import com.app.server.utils.Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RequestMapping("/api")
@RestController
public class CountHistoryController {

    @Autowired
    private ICountHistoryRepository iCountHistoryRepository;

    @Autowired
    private ICountDetailRepository iCountDetailRepository;

    @Autowired
    private IDenominationRepository iDenominationRepository;

    @Autowired
    private IRegisterRepository iRegisterRepository;

    @Autowired
    private IRegisterDenominationRepository iRegisterDenominationRepository;

    static final Logger logger = LogManager.getLogger(CountHistoryController.class.getName());

    // CREATE
    @PostMapping("checkBalance/save")
    @ResponseBody
    public ResponseEntity<CommonResponse> saveCheckBalance(@RequestBody CheckBalanceRequest checkBalanceRequest) {

        CommonResponse commonResponse = new CommonResponse();
        if (StringUtils.isNullOrEmpty(checkBalanceRequest.getDeviceId())) {
            commonResponse.setStatus(0);
            commonResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_ID_CANNOT_BE_EMPTY));
            return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
        } else if (StringUtils.isNullOrEmpty(checkBalanceRequest.getCountType())) {
            commonResponse.setStatus(0);
            commonResponse.setMessage(Utility.getMessage(AppConstants.COUNT_TYPE_EMPTY));
            return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
        } else if (checkBalanceRequest.getSystemCountValue() == 0) {
            commonResponse.setStatus(0);
            commonResponse.setMessage("System Count Value cannot be empty.");
            return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
        } else if (checkBalanceRequest.getUserCountValue() == 0) {
            commonResponse.setStatus(0);
            commonResponse.setMessage("User Count Value cannot be empty.");
            return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
        }
        if (!(checkBalanceRequest.getCountType().equalsIgnoreCase(CountType.CHECK_BALANCE.name())
                || checkBalanceRequest.getCountType().equalsIgnoreCase(CountType.MAKE_DROP.name()))) {
            commonResponse.setStatus(0);
            commonResponse.setMessage(Utility.getMessage(AppConstants.COUNT_TYPE_INVALID));
            return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
        }


        try {
            Register register = iRegisterRepository.findByDeviceId(checkBalanceRequest.getDeviceId());
            if (register == null) {
                commonResponse.setStatus(0);
                commonResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_DOES_NOT_EXISTS));
                return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
            } else if (checkBalanceRequest.getDenominations() != null && checkBalanceRequest.getDenominations().size() > 0) {
                CountHistory countHistory = new CountHistory();
                countHistory.setCountDate(new Date(System.currentTimeMillis()));
                countHistory.setSyncDate(new Date(System.currentTimeMillis()));
                countHistory.setRegisterId(register.getRegisterId());
                countHistory.setDeviceId(checkBalanceRequest.getDeviceId());
                countHistory.setCountType(checkBalanceRequest.getCountType());
                countHistory = iCountHistoryRepository.save(countHistory);

                List<CountDetails> countDetailsList = new ArrayList<>();
                int floatTotal = 0;
                for (Denominations temp : checkBalanceRequest.getDenominations()) {
                    CountDetails countDetails = new CountDetails();
                    countDetails.setCountId(countHistory.getCountId());
                    countDetails.setSyncDate(new Date(System.currentTimeMillis()));
                    countDetails.setDeviceId(countHistory.getDeviceId());
                    countDetails.setDenominationId(temp.getDenominationId());
                    countDetails.setDenominationName(temp.getDenominationName());
                    countDetails.setDenominationQuantity(temp.getDefaultQuantity());
                    countDetails.setDenominationTotal(temp.getDenominationValue() * temp.getDefaultQuantity());
                    countDetails.setDenominationValue(temp.getDenominationValue());
                    floatTotal = floatTotal + temp.getDenominationValue() * temp.getDefaultQuantity();

                    countDetailsList.add(countDetails);
                }
                if (floatTotal != checkBalanceRequest.getUserCountValue()) {
                    iCountHistoryRepository.delete(countHistory);
                    commonResponse.setStatus(0);
                    commonResponse.setMessage("Your value and user count value is different. Please check.");
                    return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
                }
                //Discuss with maruti
                countHistory.setUserCountValue(floatTotal);
                countHistory.setSystemCountValue(checkBalanceRequest.getSystemCountValue());
                iCountDetailRepository.save(countDetailsList);
                iCountHistoryRepository.save(countHistory);

                commonResponse.setStatus(1);
                commonResponse.setMessage("Check balance is successfully updated.");
                return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.OK);

            } else {
                commonResponse.setMessage(Utility.getMessage(AppConstants.DENOMINATION_LIST_EMPTY));
                return new ResponseEntity<CommonResponse>(commonResponse, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<CommonResponse>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/syncCountHistory")
    @ResponseBody
    public ResponseEntity<SyncCountResponse> syncCountHistory(String deviceId) {

        SyncCountResponse syncCountResponse = new SyncCountResponse();
        if (StringUtils.isNullOrEmpty(deviceId)) {
            syncCountResponse.setStatus(0);
            syncCountResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_ID_CANNOT_BE_EMPTY));
            return new ResponseEntity<SyncCountResponse>(syncCountResponse, HttpStatus.BAD_REQUEST);
        }
        try {
            Register register = iRegisterRepository.findByDeviceId(deviceId);
            if (register == null) {
                syncCountResponse.setStatus(0);
                syncCountResponse.setMessage(Utility.getMessage(AppConstants.DEVICE_DOES_NOT_EXISTS));
                return new ResponseEntity<SyncCountResponse>(syncCountResponse, HttpStatus.BAD_REQUEST);
            } else {
                List<CountHistory> countHistoryList = iCountHistoryRepository.findAllByRegisterId(register.getRegisterId());
                if (countHistoryList != null && countHistoryList.size() > 0) {
                    for (CountHistory temp : countHistoryList) {
                        List<CountDetails> countDetails = iCountDetailRepository.findByCountId(temp.getCountId());
                        temp.setServerId((int) temp.getCountId());
                        temp.setSynced(true);
                        for (CountDetails temp1 : countDetails) {
                            temp1.setServerId((int) temp1.getCountDetailsId());
                            Denominations denominations = new Denominations();
                            denominations.setDenominationValue(temp1.getDenominationValue());
                            denominations.setDenominationName(temp1.getDenominationName());
                            denominations.setDenominationId(temp1.getDenominationId());
                            denominations.setActive(true);
                            denominations.setDefaultQuantity(temp1.getDenominationQuantity());
                            temp1.setDenominations(denominations);
                            temp1.setSynced(true);
                        }
                        temp.setCountDetails(countDetails);
                    }
                    syncCountResponse.setCountHistoryList(countHistoryList);
                    syncCountResponse.setStatus(1);
                    syncCountResponse.setMessage("Count history fetched sucessfully");
                    return new ResponseEntity<SyncCountResponse>(syncCountResponse, HttpStatus.OK);
                } else {
                    syncCountResponse.setStatus(0);
                    syncCountResponse.setMessage("There is no count history by this device Id");
                    return new ResponseEntity<SyncCountResponse>(syncCountResponse, HttpStatus.BAD_REQUEST);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            syncCountResponse.setMessage("Bad Parameter");
            syncCountResponse.setStatus(0);
            return new ResponseEntity<SyncCountResponse>(syncCountResponse, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/syncCountHistory")
    @ResponseBody
    public ResponseEntity<CountHistory> postCountHistory(@RequestBody CountHistory countHistory) {

        try {
            countHistory.setCountId(0);
            countHistory.setSyncDate(new Date(System.currentTimeMillis()));
            if (StringUtils.isNullOrEmpty(countHistory.getCountType())) {
                countHistory.setCountType("NA");
            }
            if (StringUtils.isNullOrEmpty(countHistory.getCountRecordedBy())) {
                countHistory.setCountRecordedBy("NA");
            }
            if (countHistory.getCountDate() == null) {
                countHistory.setCountDate(new Date());
            }
            CountHistory countHistorySaved = iCountHistoryRepository.save(countHistory);
            if (countHistorySaved != null) {
                countHistory.setServerId((int) countHistorySaved.getCountId());
                countHistory.setSynced(true);
            }

            if (countHistory.getCountDetails() != null && countHistory.getCountDetails().size() > 0) {
                for (int i = 0; i <= countHistory.getCountDetails().size() - 1; i++) {
                    Denominations denominations = countHistory.getCountDetails().get(i).getDenominations();
                    if (denominations != null) {
                        countHistory.getCountDetails().get(i).setCountId(countHistorySaved.getCountId());
                        countHistory.getCountDetails().get(i).setDenominationValue(denominations.getDenominationValue());
                        countHistory.getCountDetails().get(i).setDenominationQuantity(denominations.getDefaultQuantity());
                        countHistory.getCountDetails().get(i).setDenominationName(denominations.getDenominationName());
                        countHistory.getCountDetails().get(i).setDenominationId(denominations.getDenominationId());
                        countHistory.getCountDetails().get(i).setDenominationTotal(denominations.getDenominationValue() * denominations.getDefaultQuantity());
                    }
                    countHistory.getCountDetails().get(i).setCountDetailsId(0);
                    countHistory.getCountDetails().get(i).setSyncDate(new Date(System.currentTimeMillis()));
                    CountDetails countDetailSaved = iCountDetailRepository.save(countHistory.getCountDetails().get(i));
                    if (countDetailSaved != null) {
                        countHistory.getCountDetails().get(i).setServerId((int) countDetailSaved.getCountDetailsId());
                        countHistory.getCountDetails().get(i).setSynced(true);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return new ResponseEntity<CountHistory>(countHistory, HttpStatus.OK);
    }
}